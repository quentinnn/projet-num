import easygui

#On écrit toutes les questions dans une liste.
liste_de_question = [ ' Qui a gagné la coupe du monde ? ' ,\
' Qui a été élu MVP la saison dernière en NBA ? ' ,\
' Quel est la ceinture la plus gradée au judo ? ' ,\
' Quel pays ne participe pas au tournoi des Six-Nations ? ' ,\
' Quel nom porte un terrain de tennis ? ' ,\
' Lors du Tour de France  qui est récompensé d’un maillot blanc à pois rouges ? ' ,\
' Où auront lieu les jeux Olympiques d’été en 2020 ? ' ,\
' Qu est ce que un chabala au handball ?',\
' Combien de périodes compte un match de hockey sur glace ?',\
' Quel est le nombre maximum de rameurs en Aviron ?']

#On affiche un compteur score pour que celui-ci nous donne le résultat final.
score = 0


#On donne les propositions A dans une liste.
reponsesA = [ 'Allemagne',\
              'Le Bron James',\
              'Noir',\
              'Ecosse',\
              'La terre battue',\
              'le meilleur cycliste',\
              'Japon',\
              'La balle qui passe juste au dessu de la tête du goal',\
              '2',\
              '6']

#On donne les propositions B dans une liste.
reponsesB = ['France',\
             'James Harden',\
             'Rouge',\
             'Espagne',\
             'La surface',\
             'Le meilleur jeune',\
             'Qatar',\
             'Un petit pont',\
             '3',\
             '8']

#On donne les propositions C dans une liste.
reponsesC = ['Belgique',\
             'Giánnis Antetokoúnmpo',\
             'Bleu',\
             'Italie',\
             'Le court',\
             'le meilleur grimpeur',\
             'Doha',\
             'Un grand pont',\
             '4',\
             '10']

#On fait une liste où y figure les bonnes réponses.
reponses_correctes = ['France', 'Giánnis Antetokoúnmpo', 'Noir', 'Espagne', 'Le court',
'le meilleur grimpeur', 'Japon', 'La balle qui passe juste au dessu de la tête du goal',
'3', '8']

#On crée une variable i qui va nous servir a répéter et afficher en une seule fois toutes les questions et les réponses.
i = 0
for i in range(0, len(liste_de_question)):
#On affiche les questions et ses réponses.
    easygui.msgbox(liste_de_question[i] +'\n'+ reponsesA[i] + '\n' + reponsesB[i] +'\n' + reponsesC[i])
    rep = (easygui.enterbox ("Entrée la bonne réponse :  " ))
#On crée une condition pour y afficher le résultat et de savoir si la réponse est correcte ou non.
    if rep != reponses_correctes[i]:
        easygui.msgbox('Bonne réponse: '+ reponses_correctes[i] +'\n' '-1 point...' + '\n'+ 'Réponse incorrecte' )
        score +=  -1
    elif rep == reponses_correctes[i]:
        easygui.msgbox('Bonne réponse: '+ reponses_correctes[i] +'\n' +'3 points...' + '\n' +  'Réponse correcte')
        score += 3
        
#On demande d'afficher le score.
easygui.msgbox("Ton score est de :" +str(score) + "/30")

#On écrit une fonction pour qu'en fonction du résultat l'ordinateur affiche un message différent.
def mess(score):
    if score!=30:
        if score<10:
            easygui.msgbox("Tu es nul !")
        elif 10<score<20:
            easygui.msgbox("Cest pas super...")
        elif score==30:
            easygui.msgbox("Tu es trop fort !")
        else:
            easygui.msgbox("Pas mal !")
    return(score)
        

        
        
        

        
