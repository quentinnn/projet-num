Mon projet est un quiz où j'y est crée 10 questions de culture générale
sur différents sports.

Les règles du quiz:
Ce quiz comprend 3 propositions, il n'y a qu'une bonne réponse.
Vous devrez répondre en recopiant la réponse que vous penser être la bonne.
Une bonne réponse vaut 3 points, mais une mauvaise -1 point.

Au début j'ai commencé à chercher différentes questions que je pourrais poser.
Je l'es ai mise sous forme de liste. J'ai fait de même avec les propositions et
fait une liste spécifique pour les bonnes réponses.

Mon programme se trouve dans une boucle for i in range pour que l'ordinateur puisse afficher
les questions et les reponses toutes en mêmes temps. Cela permet d'éviter les 
repetitions d'un même programme. Dans cette boucle se trouve l'affichage des questions et des 
propositions. J'y est aussi integré des conditions pour afficher si la réponse
est correcte ou non ainsi qu'un système de point pour pouvoir afficher un score
à la fin. Toute cette boucle est sous la forme de easygui.

Pour finir j'ai voulu qu'un message s'affiche en fonction du score. J'ai
donc crée une fonction composée de  if, plusieurs elif et else pour qu'un message 
s'affiche. Etant donné que c'est sous forme de fonction, après le quiz
je dois écrire dans le terminale: mess(le score que l'on viens de faire).

J'ai rencontré quelques difficultés dans mon programme:

Tout d'abord, il me manquait quelques questions pour avoir assez de question
pour le quiz, j'ai donc chercher mes dernieres questions sur des quiz
de sport que j'ai trouvé sur DuckDuckGo.

Ensuite, j'ai voulu créer une variable pour les mauvaises réponses et pour les
bonnes réponses mais j'avais un probleme pour afficher le score final
car cela me donnait le nombre de bonnes réponses et de mauvaises réponses
alors que je voulais juste le scoore final. J'ai donc créer une variable
score qui enleve et rajoute les points du score.

Puis, dans la boucle finale qui permet d'annoncer un message selon le score
au debut j'ai testé avec while sauf que le score s'afficher plein
de fois, j'ai donc mis un simple if pour que le message ne s'affiche qu'une
seule fois.

Pour finir quand je mettais les conditions pour savoir si la réponse
est vraie ou fausse,apres mon if, else ne fonctionnait pas,
il m'affichait les deux réponses. Je l'es donc
remplacer par un elif et le programme a fonctionné.

J'ai choisi ce projet car j'aime tous ce qui est autour du sport, je veux même en faire mon métier
J'ai aimé faire ce projet car le choix etait libre j'ai donc pu faire ce que j'ai aimé.